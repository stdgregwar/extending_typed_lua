#+TITLE: Extending typed lua
#+OPTIONS: num:3 H:4 ^:nil pri:t

#+SETUPFILE: https://fniessen.github.io/org-html-themes/setup/theme-readtheorg.setup
##+HTML_HEAD: <link rel="stylesheet" type="text/css" href="org.css"/>

#+BEGIN_abstract
This journal compiles my work on the extension of typed lua as a master semester project
at EPFL. I summaries my attempts at type-checking two constructions of Lua : co-routines
and function environment modification with ~setfenv~.  
#+END_abstract



* Entry points ideas
** Typing coroutines (~easy?)
*** DONE : Formulate problem and find example
     CLOSED: [2019-03-05 Tue 13:09]
** Typing function envirronement and module loading
*** DONE : See how typed lua does it at this point.
    CLOSED: [2019-03-05 Tue 19:17]
*** DONE : Formulate problem and find example
     CLOSED: [2019-03-05 Tue 13:09]

* First typed_lua tests
** Coroutines

Co-routines are not type-able because type of the returned co-routine object depends
on the given function type. This could be addressed by merging Kevin Clancy's F-Bounded
genericity system. see [[https://kevinclancy.github.io/2016/08/18/a-class-system-for-typed-lua.html][On kevin's blog]].

** Globals and envirronment
*** First simple cases
First test with Typed Lua Compiler (TLC) was this code snippet :

#+BEGIN_SRC lua
print('hello world')


local function foo()
  print = function(arg : number) assert(type(arg) == 'number', 'runtime type error') end
end

foo()

print('hello world')
#+END_SRC

This first test shows that TLC is able to protect global symbol from type changes.
It indeed produce a compile-time type error :

#+BEGIN_SRC
env_test.tl:8:3: type error, attempt to assign '((number, value*) -> ())' to '((value*) ->
 (), value*)'                                                                            
  print = function(arg : number) assert(type(arg) == 'number', 'runtime type error') end
  ^
#+END_SRC

But note that this slight change in the source code makes it compile without any warnings :

#+BEGIN_SRC lua
print('hello world')

local function foo()
  print = function() end
end

foo()

print('hello world')
#+END_SRC

Because the default behaviour of TLC is to reflect Lua's behaviour of not checking function
arity before calling. See [[http://www.lifl.fr/dyla14/papers/dyla14-4-typed-lua-an-optional-type-system.pdf][Typed lua paper]].
To be fair, this code generate the following warning if we explicitly enable them :

#+BEGIN_SRC
> tlc -w env_test.tl
env_test.tl:8:3: warning, attempt to assign '((any, value*) -> ())' to '((value*) -> (), v
alue*)'                                                                                  
  print = function(arg) assert(type(arg) == 'number', 'runtime type error') end
  ^
#+END_SRC

*** What if we try to extend global env ?

When writing code, although it is considered bad practice, one could declare new global symbols. Let's see how typed lua behaves on this.

This code :

#+BEGIN_SRC lua
function foo(a : number, b : string)
  print(a,b)
end

foo(42, 'The question')

function foo(a : string, b : number)
  print(a,b)
end

foo(42, 'The question')
#+END_SRC

Detects the modification of signature correctly :

#+BEGIN_SRC
> tlc new_globals.tl
new_globals.tl:7:10: type error, attempt to assign '((string, number, value*) -> ())' to '((number, string, value*) -> (), value*)'
function foo(a : string, b : number)
         ^
#+END_SRC

Let's see how declaration order matters. This hopefully compiles :

#+BEGIN_SRC lua
function foobar(a : number, b : string)
end

function foo(a : number, b : string)
  foobar(a,b)
end

foo(42, 'The question')
#+END_SRC

But this potentially working example does not :

#+BEGIN_SRC lua
function foo(a : number, b : string)
  foobar(a,b) -- foobar is an undeclared global variable at this point
end

function foobar(a : number, b : string) -- but it is defined before 
end

foo(42, 'The question') -- this call site so this should be fine
#+END_SRC

#+BEGIN_SRC
 > tlc new_globals.tl
new_globals.tl:2:3: type error, attempt to access undeclared global 'foobar'
  foobar(a,b)
  ^
new_globals.tl:2:3: type error, attempt to call global 'foobar' of type 'nil'
  foobar(a,b)
  ^
#+END_SRC

An obvious solution to this problem is to do a forward declaration of the foobar function
in the environment.

But what if a function tries to extend the global env from its own scope ?


#+BEGIN_SRC lua
local function make_foobar()
  function foobar(a : number, b : string)
  end
end

make_foobar()

function foo(a : number, b : string)
  foobar(a,b)
end

foo(42, 'The question')
#+END_SRC

This "correctly" fail

#+BEGIN_SRC
> tlc new_globals.tl
new_globals.tl:11:12: type error, attempt to access undeclared global 'foobar'
  function foobar(a : number, b : string)
           ^
new_globals.tl:11:12: type error, attempt to assign '((number, string, value*) -> ())' to '(nil, value*)'
  function foobar(a : number, b : string)
           ^
new_globals.tl:18:3: type error, attempt to access undeclared global 'foobar'
  foobar(a,b)
  ^
new_globals.tl:18:3: type error, attempt to call global 'foobar' of type 'nil'
  foobar(a,b)
  ^
#+END_SRC

*** Files and modules loading

Here we explore the way typed lua currently handles file loading/doing and modules requiring.

Let's start with a very simple example.

We declare a simple lib in a `require_me.tl` file.

#+BEGIN_SRC lua
local sample_lib = {}

function sample_lib.print(a : number, b : string)
  print(a,b)
end

return sample_lib
#+END_SRC

Note that this lib is written in modern lua style

And then try exec it like so:

#+BEGIN_SRC lua
local test = dofile('require_me.tl')

test.print('error ?', 'no error ?')
#+END_SRC

Which give only some warnings, it appears that typed lua treat
dofile result as a fully dynamic value. Which is fine as it is
often the right usage for dofile.

#+BEGIN_SRC
> tlc -w do_file.tl
do_file.tl:3:1: warning, attempt to index 'any' with '"print"'
test.print('error ?', 'no error ?')
^
do_file.tl:3:1: warning, attempt to call field 'print' of type 'any'
test.print('error ?', 'no error ?')
^
#+END_SRC

Know if we do the same but use `require` instead of `dofile` we get the
expected behaviour :

#+BEGIN_SRC lua
local test2 = require('require_me')

test2.print('error ?', 'no error ?')
#+END_SRC

And we now get an error for the incorrect use of the print function declared in
`require_me.tl`.

#+BEGIN_SRC
> tlc -w do_file.tl
do_file.tl:7:1: type error, attempt to pass '("error ?", "no error ?")' to field 'print' of input type '(number, string, value*)'
test2.print('error ?', 'no error ?')
^
#+END_SRC

So it seems that typed lua correctly registers returned types of our small module script.
Perfect ! Or is it ?

What if our required script create globals that were not defined previously ?

What if our script defines some symbols before requiring the module ?

First, we modify our module script to use some global symbol foobar :

#+BEGIN_SRC lua
local sample_lib = {}

function sample_lib.print(a : number, b : string)
  print(foobar(a), b)
end

return sample_lib
#+END_SRC
Then we define this symbol in our main script and require

#+BEGIN_SRC lua
function foobar(me : number) : number
  return me
end

local test2 = require('require_me')

test2.print(42, 'no error ?')
#+END_SRC

Which compiles as expected.

And the incorrect following code

#+BEGIN_SRC lua
local test2 = require('require_me')

function foobar(me : number) : number
  return me
end

test2.print(42, 'no error ?')
#+END_SRC

Raise a compile time error, which is sound.

#+BEGIN_SRC
 tlc -w do_file.tl
./require_me.tl:4:9: type error, attempt to access undeclared global 'foobar'
  print(foobar(a), b)
        ^
./require_me.tl:4:9: type error, attempt to call global 'foobar' of type 'nil'
  print(foobar(a), b)
        ^
#+END_SRC

One last test that we can think of is the correct registration of global
 symbols from the module.

We modify our module once more to add a global symbol :

#+BEGIN_SRC lua
local sample_lib = {}

function sample_lib.print(a : number, b : string)
  print(foobar(a), b)
end

function foo(str : string) : string
  return str .. '_suffix'
end

return sample_lib
#+END_SRC

and our other script to use this symbol

#+BEGIN_SRC lua
function foobar(me : number) : number
  return me
end

local test2 = require('require_me')

local res = foo('a test')

test2.print(42, 'no error ?')
#+END_SRC

The compilation fails with the following error :

#+BEGIN_SRC
> tlc -w do_file.tl
./require_me.tl:7:10: type error, attempt to access undeclared global 'foo'
function foo(str : string) : string
         ^
./require_me.tl:7:10: type error, attempt to assign '((string, value*) -> (string))' to '(nil, value*)'
function foo(str : string) : string
         ^
do_file.tl:11:7: warning, unused local 'res'
local res = foo('a test')
      ^
do_file.tl:11:13: type error, attempt to access undeclared global 'foo'
local res = foo('a test')
            ^
do_file.tl:11:13: type error, attempt to call global 'foo' of type 'nil'
local res = foo('a test')
            ^
#+END_SRC

So we can deduce that the global record `_G` is considered "closed"
(like in functions scopes) in required files. Which seem reasonable.

*** Running environment

This lovely lua program run twice the same function two different environments

#+BEGIN_SRC lua
name = 'A'

local function foo()
  print("Hello world from env", name)
end

foo() -- call foo from the normal 'A' env

setfenv(foo,{print=print, name='B'})

foo() -- call foo from the restricted 'B' env
#+END_SRC

Here setfenv is part of the lua 5.1 standard library and change the global record
where the globals come from.

One can convince himself that the above code is correct and will output the following :

#+BEGIN_SRC
Hello world from env    A
Hello world from env    B
#+END_SRC

And the TLC will happily type-check this program... so.... what is wrong ?

Let's modify the program a bit

#+BEGIN_SRC lua
name = 'A'

local function foo()
  print("Hello world from env", name)
end

foo() -- call foo from the normal 'A' env

setfenv(foo,{name='B'}) --now print is MISSING ON PURPOSE

foo() -- call foo from the restricted 'B' env
#+END_SRC

Know we see the problem, the function foo will be called with the environment B
that lacks the `print` global and code in foo() becomes ill-formed.

However, TLC still compiles happily but the program crash...

So, how could we detect this error at type-check time ?

* Type checking setfenv
** DONE Intuitive solution
   CLOSED: [2019-03-07 Thu 11:31]

An intuitive solution would be to make the environment bound to the function part of the
type of this function. This way we ensure that code trying to modify the bound environment
does it with a record that is covariant of the already bound record.

But this immediately raise a problem, functions tend to use a very sparse amount of the symbols
defined in the environment. So when one would like to replace an environment chances are that
he will define only the necessary symbols. This method fails in this case because the a 
record with less symbols cannot be covariant with a bigger one.

Another problem raised by this method is that it implies that the function type is mutable
by the control flow. Which make the problem several order of magnitude
 harder to reason about.

** DONE More Correct solution
   CLOSED: [2019-03-07 Thu 11:56]

To address the issue of the previous method. We propose to compute for the function the
set of used symbols along with their types. If we use this minimal compatible record (MCR)
to do comparison when checking the function then we can bound functions to much smaller
environment than with the previous method.

Checking by ensuring that ~setfenv~ is always called with compatible envs we ensure that
the function will always be safe to call. Or do we ?

** What if the function is declared in an incompatible environment ?

Consider the following code :

#+BEGIN_SRC lua -n 1
local function run_in_funny_env(closure)
  setfenv(closure, {log=print})
  closure()
end

run_in_funny_env(function()
  log('hello world!')
end)
#+END_SRC

This code runs fine in lua. But how do we ensure it is correctly typed ? The culprit here
is that the anonymous function at line 6 is defined in the standard environment. With
which it is incompatible. We could tag function as not callable. And it would become
callable again when setfenv is used. This seems simple in this case but is hard if there
is control flow involved.
** Compatibility Problem

One big issue that we weren't aware of when starting thinking about typing ~setfenv~ is that
it is a lua 5.1 only construction. Lua 5.2 and later use a ~_ENV~ up-value as function environment. 
Typing such primitive, considering how much work it will be, is maybe not an optimal choice since
it will profit only to lua 5.1 and ~LuaJIT~ users.

** Conclusion

We choose to not give priority on the setfenv since it is both challenging and a "deprecated" feature.

* Type checking co-routines
** The coroutine library

Coroutines are part of the lua standard library and are a way of achieving 
non-preemptive multi-threading. The ~coroutine~ library offers various 
primitives to achieve asymmetric or symmetric collaborative tasks and
can be summarized like so :

#+CAPTION: ~coroutine~ library, already annotated with type variables
#+BEGIN_SRC lua
userdata thread[P,R] end

-- P = (P1, P2, ...)
-- R = (R1, R2, ...)

-- allocates a new thread that will run the given function
create : ((P1,P2, ...) -> (R1,R2, ...)) -> (thread[P,R])

-- start or resume a thread execution, giving it some values,
-- values yielded from the thread are returned
resume : (thread[P,R], P1, P2, ...) -> (boolean, string|R1, R2, ...)

-- gets the running thread or nil if called on the main thread
running : () -> (thread[P,R]?)

-- gets the status of a thread
status : (thread[P,R]) -> (string)

-- wrap a coroutine created with given function in another
-- function with the same parameters
wrap : ((P1, P2, ...) -> (boolean, string|R1, R2, ...))
    -> ((P1, P2, ...) -> (boolean, string|R1, R2, ...))

-- suspend current thread and give values to the calling `resume`
-- call, next resume call values are returned 
yield : (R1, R2, R3) -> (P1, P2, ...)
#+END_SRC 

As we can see. The ~coroutine.create~ function allocate a ~thread~ userdata object. The types returned or
expected from the subsequent ~coroutine~ calls depend on the parameter and return types of the function
wrapped into the ~thread~ object.

So, to properly type the coroutines, we need type parameters and type parametrized objects (or userdata in this
case).

Following is an example of coroutine usage
#+CAPTION: plain lua coroutine example
#+BEGIN_SRC lua -n 1
local coro = coroutine

local function coro_test(a,b,c)
  print(a, b, c)
  local g, h, i = coro.yield('d','e','f')
  print(g, h, i)
  return 'j', 'k', 'l'
end

local thread = coro.create(coro_test)

local s,d,e,f = coro.resume(thread, 'a', 'b', 'c')
print(s,d,e,f)
local s,d,e,f = coro.resume(thread, 'g', 'h', 'i')
print(s,a,b,c)
local s = coro.resume(thread)
print(s)
#+END_SRC

Which gives the following output.

#+BEGIN_SRC
a	b	c
true	d	e	f
g	h	i
true	nil	nil	nil
false
#+END_SRC

** Requirements

As said previously we need type parameters and type parametrized objects to type the coroutines functions
and threads. 

Because the function given to ~coroutine.create~ can have any arity. It would be more flexible to have type
parameters packs. Like in ~c++11~ where templates can be parametrized with packs like this :

#+BEGIN_SRC cpp
template<class ...Us> void f(Us... pargs) {}
template<class ...Ts> void g(Ts... args) {
    f(&args...); // “&args...” is a pack expansion
                 // “&args” is its pattern
}
g(1, 0.2, "a"); // Ts... args expand to int E1, double E2, const char* E3
                // &args... expands to &E1, &E2, &E3
                // Us... pargs expand to int* E1, double* E2, const char** E3
#+END_SRC

From this simple observation, we decide to try merging Kevin Clancy's work on type parameters and parametric types systems into the trunk typed-lua.
This will most probably be challenging since those two branches seem to have diverged a lot.

Our method will be to incrementally re-implement Kevin's work in the actual typed-lua trunk. Trying to preserve actual behavior as much as possible.

** TODO On the theory side
*** TODO Determine the scope of ~co-routine~ type checking
*** TODO Find informal rules for deducing a co-routine's type and check it
*** TODO Formalize co-routine type checking rules

** TODO In parallel, proposed implementation road-map
*** DONE Merge basic parametric type syntax parsing (excluding classes and F-Bounded type parameters)
    CLOSED: [2019-03-19 Tue 15:46]
*** DONE Fix AST structure to take Type Variables and other needed constructs into account 
    CLOSED: [2019-03-19 Tue 15:46]
*** DONE Port basic Generic Functions instances factory and type checking routines
    CLOSED: [2019-03-19 Tue 15:46]
*** TODO Allow parametric interfaces, userdata and type aliases
*** TODO Add co-routine checking as a primitive in typed-lua
*** Other extensions if time permits
* Adding Genericity    
** local functions implementation, template style
A first step towards having type parameters at many levels into the type system was to test
implementation on simple symbols. The simplest parameterize-able types I could think of where
functions. Since global functions are fields in the the ~_G~ table, I chose to only allow
type parameters in local functions as a first step.


*** Parsing

We added an optional branch in the abstract syntax tree. Right before a function body
~(...) ... end~ that matches a list of variables ~<A, B, ...>~. So that we can now parse
parametric function that look like this ~local function foo<A,B>(....) ... end~. We then
added a new optional branch on the variable expressions (identifiers) so that theses can
receive type parameters (~foo<number, string>~). Those type parameters become a property
of the *VarExpr*.

*** Type-checking

While type checking, when we stumble on a local functions that have type parameters, we
don't directly type-check them. Instead, they are given a *GenericFunction* type that
carries the function definition and a snapshot of the checking environment corresponding
to the scope in which they are defined. Later, when the function is used as an identifier
in expression, we check that the right amount of type parameters are passed (we don't do
type inference for generic functions parameters yet) and then proceed
to type-check the function body with the type variables defined as type-alias for the
concrete types of the instance.

*** Problems

Using an environment snapshot while checking the function instance prevent type-alias defined
later in child scopes to get used as type arguments. Or they need their names to be substituted
with concrete type before being substituted into the function's body... which could
defeat the purpose of naming types..... On the other hand, having the function checked
on the instancing or call-site would break some lua semantics of nested scopes. Namely,
a function could call one defined in a deeper scope.
** How many syntactic sugars in your tea ?

Taking a short break with type checking. Since I was gaining confidence into my ability to
edit the grammar of *TypedLua*. I wondered if this was possible to achieve the secret dream
of many lua users. Having a short syntax for anonymous function.

I chose to limit it to closure with a single expression list that directly serve as a
return statement.

#+CAPTION: example demonstrating use of lambda function syntax
#+BEGIN_SRC lua
local function normal(a : number, b :number)
  return a + b, a-b
end

local magic = (a : number, b : number) -> a+b, a-b

normal(2,10)
magic(2,10)
#+END_SRC

This will allow us to have more concise code examples from now on.
** local function implementation, real deal

Using C++ template-style semantics for the type parametric features is maybe a form of overkill
and can give lots of confusing errors to the users. It does not allow user to fix clear
bounds on parameters quality. That's why we changed our mind and implemented genericity
in a more common way. Type parameters are considered as a special type ~TParameter~ that can
have optional type bounds and function body is checked in isolation directly when traversing
the function definition. This is both safer and clearer from an error message point of view.

One great side-effect of this is that it allows to encapsulate dynamically typed parts or unsafeness
into safe parametric blocs.

#+CAPTION: toy example demonstrating the actual state of genericity
#+BEGIN_SRC lua -n 1
local function id<U>(arg : U) : U
      return arg
end

local function map<U, V>(array : {U}, f : (U) -> (V)) : {V}
      local res : {V} = {}
      for i, v in ipairs(array) do
          res[i] = f(v)
      end
      return res
end

local us : {number} = {1,2,3,4,5}
local vs = map<number,string>(us, tostring)

local dus = map<number, number>(us, (n : number) -> 2*n)

local curry = id<string>

print(curry("Hello"))
#+END_SRC

The toy example type-checks with the following warnings :

#+BEGIN_SRC
examples/gsimple.tl:7:19: warning, attempt to pass '({integer: U?})' to global 'ipairs' of input type '({integer: any})'
      for i, v in ipairs(array) do
                  ^
examples/gsimple.tl:8:20: warning, attempt to pass '(any)' to local 'f' of input type '(U)'
          res[i] = f(v)
                   ^
examples/gsimple.tl:14:7: warning, unused local 'vs'
local vs = map<number,string>(us, (n : number) -> tostring(n))
      ^
examples/gsimple.tl:16:7: warning, unused local 'dus'
local dus = map<number, number>(us, (n : number) -> 2*n)
      ^
#+END_SRC

We notice that we got warning because we cross the untyped parts of the standard on lines ~7~ and ~8~. But this will
be solved when we port our genericity to the ~tld~ files. At this point we will be able to type ~ipairs~ correctly and
have strong guaranties about the body of our function ~map~.

** minimal type inference

In the previous section, we presented how we achieved genericity for local function
definitions. But, although they were deducible, the type arguments had to be explicited
on the generic instanciation site. To address this hassle, we provide a minimalistic
type inference system.

When an symbol is used with type argument (~symbol<TA,TB,TC>~ for example) the transpiler
checks that symbol has a ~TGeneric~ type and that its number of type parameters matches
the number of arguments passed and the type is instanced. However, when the symbol has 
type ~TGeneric~ but is not provided type arguments, it's type stays TGeneric and the
further expressions have to deal with it. A subset of expression, such as calls, handle
the case where the given called expression has type ~TGeneric~ and use a type inference routine
to deduce the needed types of the arguments.

** Generic types in ~tld~ files

Typed lua description files are a way to give a typed front-end to an untyped, plain-lua or
C-lua library. They are also used to define the types of the standard library symbols such
as ~ipairs~.

*** ~ipairs~ example

~ipairs~ is a lua built-in function that takes a table and returns an iterator that yields
the table array-part entries in order zipped with their index (starting at one). If called
on a table of type ~{E}~, pairs yielded by this iterator are guaranteed to have type
~(number, E)~ (or ~(integer, E)~ in lua 5.3). However, current entry for the symbol ~ipairs~
in the ~base.tld~ file embedded in the transpiler is :

#+BEGIN_SRC
ipairs : ({any}) -> (({any}, integer) -> (integer, any), {any}, integer)
#+END_SRC

So, even if type of the array part of the table that we iterate over is known. It is lost when it's
being iterated over.

Hopefully we were able to have type parameters in tld files as well. This allow us to write
~ipairs~ signature like so :

#+BEGIN_SRC
ipairs : <U>({U}) -> (({U}, integer) -> (integer, U), {U}, integer)
#+END_SRC

Which allow our previous example ~map~ function to compile without warnings. It was a
good surprise to see that TypedLua already handles correctly the type of the variables
in the for loop body given an iterator.

** Generic Interfaces

On our way to make generic containers or data structures possible in TypedLua, we have to
allow for parametric interfaces, like in this small example :

#+BEGIN_SRC lua
local interface Box<U>
  val : U
end

local interface Pair<A, B>
  first : A
  second : B
end

local typealias PairWithNum<B> = Pair<number,B>

local function box<U>(val : U) : Box<U>
      return {val = val}
end

local function check_num_pair(pair : PairWithNum<string>) : boolean
      return pair.first == tonumber(pair.second)
end

local function unbox<U>(box : Box<U>) : U
  return box.val
end

local function check_num(a : number) print(a) end
local val = unbox(box(42))
check_num(val)
#+END_SRC 

For this, we once again modified the parser to account for type parameters in interfaces
and typealias definitions. And beside type variable to allow to instanciate generic types
that are put into variables.


** TODO Typing more of the standard library ?

To give an idea of the amount of work remaining. Here is the actual state of the ~base.tld~
file containing essential lua library symbols.

#+BEGIN_SRC
--[[
Typed Lua description file for basic functions
]]

arg : {string}

assert : $assert
collectgarbage : (string?, integer?) -> (number|boolean)
dofile : (string?) -> (any*)
error : $error
_G : { string:any }
getmetatable : (value) -> (any)
ipairs : <U>({U}) -> (({U}, integer) -> (integer, U), {U}, integer)
load : (string|(any*) -> (any*), string?, string?, any) -> (() -> (any*))?
loadfile : (string?, string?, any) -> (() -> (any*))?
next : ({any:any}, any) -> (any, any)
pairs : ({any:any}) -> (({any:any}) -> (any, any), {any:any})
pcall : ((any*) -> (any*), any*) -> (boolean, any*)
print : (value*) -> ()
rawequal : (value, value) -> (boolean)
rawget : ({}, any) -> (any)
rawlen : ({}|string) -> (integer)
rawset : ({any:any}, any, any) -> ({any:any})
select : (integer|string, value*) -> (any*)
setmetatable : $setmetatable
tonumber : (value, integer?) -> (number) | (nil)
tostring : (value) -> (string)
type : $type
_VERSION : string
xpcall : ((any*) -> (any*), any, any*) -> (boolean, any*)

require : $require
#+END_SRC

If we remove the ones that are clearly either reflexive or whose type depend on dynamic state,
and those that types already well, we are left with a few symbols that we could hope to type 
correctly using genericity.

#+BEGIN_SRC
--[[
Typed Lua description file for basic functions
]]

ipairs : <U>({U}) -> (({U}, integer) -> (integer, U), {U}, integer)
next : ({any:any}, any) -> (any, any)
pairs : ({any:any}) -> (({any:any}) -> (any, any), {any:any})
pcall : ((any*) -> (any*), any*) -> (boolean, any*)
rawset : ({any:any}, any, any) -> ({any:any})
select : (integer|string, value*) -> (any*)
xpcall : ((any*) -> (any*), any, any*) -> (boolean, any*)
#+END_SRC

In our way to type coroutines, having tools to express how output parameters vary with respect
to input parameters would be a must have. Having type parameters packs could be very useful.

For example, this could mean the following prototype for ~pcall~, (protected call):

#+BEGIN_SRC
pcall : <...A,...B>((...A) -> (...B), ...A) -> (boolean, string | ...B)
#+END_SRC

** Emacs major mode

While trying to write more and more complex examples of TypedLua code, it appeared to be 
time consuming to take care of indentation and syntax by hand. The need for auto-completion
and live syntax highlighting and checking grew stronger everyday.

We took the liberty to fork ~lua-mode~ to take TypedLua syntax into account. Which lead to
~tl-mode~. As an addition, we modified the compiler CLI tool ~tlc~ to make ~tlcheck~ the
tl equivalent of luacheck. This permit to have correct syntax highlight and live type checking
and error reporting. This really is a nice to have and we are sure that it will greatly
improve our productivity.

** Type inference formalization
*** Unsound first try
The minimalist type inference algorithm that we should use can be summarized with the following pseudo code :

#+BEGIN_SRC bash
# P : parametric type template
# G : given type
# S : substitution from TypeParameters to types
# R : <: or :>, subtyping direction
infer(P, G, S, R) =
  if P = G
    then S
  else if P is TParam X and X not in S
    then S o [X -> G]
  else if P is TParam X and X in S
    then let Tx be S(X) in
      if G R Tx # g co(contra)variant with already deduced type
        then S
      else if Tx relation G # Tx contra(co)variant with already deduced type
        then S o [X -> given] # update substitution to broader type
      else
        fail # could not concile given type with previously deduced one
  else if P = P1->P2 and G = G1->G2 # functions
    then let S1 = infer(P1, G1, S, ~ R) in
      infer(P2, G2, S1, R)
  else if P = P1*...*PN and G = G1*...*GN # tuples
    then foldLeft(zip(P,G), S,
                  (Sn, (Pn, Gn)) => infer(Pn, Gn, Sn, R))
  else if P = P1+...+PN and G = G1+...+GN # unions
    then foldLeft(zip(P,G), S,
                  (Sn, (Pn, Gn)) => infer(Pn, Gn, Sn, R))
  else if P = {P1:P2?} and G = {G1:G2?} # hashs
    then let S1 = infer(P1, G1, S, R) in
      infer(P2, G2, S, R)
  else if P = {KP1:VP1,...,KPn:VPn} and G = {KG1:VG1,...,KGn:VGn} # records
     then infer each K:V pair recursively
  else
    fail
#+END_SRC

Not that this only almost reflects the actual implementation.

We can already see some similitude with the *unification* algorithm. The major difference is that
we don't unify a set of type constraints corresponding to a whole program but only a small set of
constraint corresponding to a function application. So the type information flows naturally from
already typed arguments to to be deduced return types of the function. The usage of the algorithm
is really simple. We get an argument tuple and a generic function type from the program. We take
the input tuple of the generic function as ~P~ and the argument tuple as ~G~. We start with an
empty ~S~ and a covariant relation ~R~ (~<:~). The infer algorithm yield either an error or a 
substitution that can be used to instantiate the function and deduce arguments. 

*** Possible improvements

We can already see that in the case of records we can't infer an plausible intersection. Id est,
a narrower record type could be deduced that is a super-type of both records.

#+BEGIN_SRC tl
local function pair<U>(a : U, b : U)
   return a,b
end

-- is ok, b is wider than a
pair({c=1}, {c=1, a=2})

-- is ok, a wider than b
pair({a=1, b=1}, {a=1})

-- fails, altough actual instatiation is possible
pair({c=1, a=1}, {c=1, b=1})

-- manual instance works
pair<{"c":integer}>({c=1, a=1}, {c=1, b=1})
#+END_SRC

So there is indeed room for improvement. In the case of record we could allow to narrow the width
of the deduced type.

*** Hopefully sound version

After reading "Local Type Inference - PIERCE & TURNER". I have better understanding of the pitfalls
in which I stumbled across with the previous version of the inference algorithm. A more correct version
operate in two steps. First a mapping from TypeParameters to Type bounds is generated. Then bounds
are transformed to concrete types depending on the place the TypeParameter occupy in the generic template.

Type bounds need some maximal and minimal types. Traditionnaly those are ~Top~ and ~Bot~ but in 
TypedLua ~Top~ is ~Value~ and there is no bottom type... we introduce ~Nothing~ that will serve as a bottom type.

#+BEGIN_SRC bash
# P : parametric type template
# G : given type
# B : substitution from TypeParameters to type bounds, start with S = [Nothing <: X <: Value] for all X
# R : <: or :> or invariant, subtyping relation,
infer_bounds(P, G, B, R) =
  if P = G
    then S
  else if P is TParam X
    then let [S <: X <: T] be B(X) in
      if R is <:
        then if G <: S
          then B
        else if S <: G
          then B o [G <: X <: T]
        else
          fail
      if R is :>
        then if G :> T
          then B
        else if T :> G
          then B o [S <: X <: G]
        else
          fail
      if R is invariant
        then if S <: G <: T
          then B o [G <: X <: G]
        else
          fail
  else if P = P1->P2 and G = G1->G2 # functions
    then let B1 = infer_bounds(P1, G1, B, ~ R) in
      infer_bounds(P2, G2, B1, R)
  else if P = P1*...*PN and G = G1*...*GN # tuples
    then foldLeft(zip(P,G), B,
                  (Bn, (Pn, Gn)) => infer_bounds(Pn, Gn, Bn, R))
  else if P = P1+...+PN and G = G1+...+GN # unions
    then foldLeft(zip(P,G), B,
                  (Bn, (Pn, Gn)) => infer_bounds(Pn, Gn, Bn, R))
  else if P = {P1:P2?} and G = {G1:G2?} # hashs
    then let B1 = infer_bounds(P1, G1, B, R) in
      infer_bounds(P2, G2, B1, R)
  else if P = {KP1:VP1,...,KPn:VPn} and G = {KG1:VG1,...,KGn:VGn} # records
     then infer each K:V pair recursively, key should be invariant...
  else
    fail
#+END_SRC

The algorithm is called with the two input type tuples : the generic tuple ~P~ (containing
type parameters) and the given tuple ~G~ with which the function was called on the
instantiation site. Type bounds map ~B~ is initialized with infinite bounds for each
parameters. Relation ~R~ is covariant ~<:~.

If algorithm does not fail, the obtained bounds still need to be checked and proper
instantiation has to be defined for each parameters. This is done as described in "Local type inference".

#+BEGIN_SRC bash
# P : parametric type of the function
for each [S <: X <: T] in B
  assert(S <: T)
  if P is covariant or constant in X:
    then X -> S
  else if P is contravariant in X:
    then X -> T
  else if P is invariant in X and S=T:
    then X -> S
  else
    fail
#+END_SRC 

*** Least upper bound & Greatest lower bound

There are case where we could want to deduce a parent type of two
different types. For example :

#+BEGIN_SRC tl
local function pair<U>(a : U, b : U) return a, b end

pair({x = 1, y = 2}, {x = 3, z = 'a'}) -- U should be {x : integer}
#+END_SRC

Here we intuitively find that the solution is an intersection of given records.

Current implementation does this but only in the covariant case (intersection).
in the contravariant case this would require an union.

*** TODO rewritten version including intersection_types

* Thoughts on TypedLua
** Mutual recursion

As in several languages, lua way to provide mutual recursion is via forward declaration

#+CAPTION: A forward declared lua mutual recursion
#+BEGIN_SRC lua
-- g is declared as a nil local variable
local g

local function f(i)
  return g(i) -- g is captured as an upvalue
end

-- g is assigned a function value
function g(i)
  return f(i) -- g is captured as an upvalue
end
#+END_SRC 

#+CAPTION: The same example in TypedLua
#+BEGIN_SRC tl
-- g is forward declared with his function type
local g : (number) -> (number)

local function f(i :number) : number
  return g(i) -- error : g's type is (number) -> (number)?
end

function g(i : number)
  return f(i)
end
#+END_SRC

It would be interesting to find some way to ensure that g is defined when used in f...

*** Simple algorithm to allow forward declaration as locals

Typed lua assign two different types to the local variables. A concrete type and ~ubound~
which represent the type of the variable once bound as an upvalue. The previous behaviour
was to make ~ubound~ of type ~T?~ when declared of type ~T~. In a new implementation,
~ubound~ is set to the full type ~T~ but a check is made at the end of the basic block
to ensure that the forward declared variable has been assigned the right type before any
of the functions could run.

** Types union and type traits

TypedLua support union of type with the syntax ~type1 | type2 | type3~. For tables, this
tells the type checker that the type is *either* type1 or type2 or type3. This already
very useful as it permits the formation of sum types that can be named :

#+CAPTION: declaration of a union typealias
#+BEGIN_SRC tl
local typealias Type123 = Type1 | Type2 | Type3
#+END_SRC

Those union types can then be discriminated using either the lua type tags or literal
table fields tags, like in this example from Andre Murbach Maidl thesis :

#+CAPTION: declaration and usage of a tagged union
#+BEGIN_SRC tl
local typealias Exp = {"tag":"Number", val:number}
                    | {"tag":"Add", lhs:Exp, rhs:Exp}
                    | {"tag":"Sub", lhs:Exp, rhs:Exp}
                    | {"tag":"Mul", lhs:Exp, rhs:Exp}
                    | {"tag":"Div", lhs:Exp, rhs:Exp}

local function eval (e: Exp):number
  if e.tag == "Number" then return e.val
  elseif e.tag == "Add" then return eval(e.lhs) + eval(e.rhs)
  elseif e.tag == "Sub" then return eval(e.lhs) - eval(e.rhs)
  elseif e.tag == "Mul" then return eval(e.lhs) * eval(e.rhs)
  elseif e.tag == "Div" then return eval(e.lhs) / eval(e.rhs)
  end
end
#+END_SRC

One can see this as some primitive form of Pattern Matching.

What is missing is some way to combine different interfaces like traits.

We could imagine this new syntax to declare a type implementing several traits

#+CAPTION: proposed syntax for type traits union
#+BEGIN_SRC tl
local interface Addable<U>
  add : (U) => (U) -- method taking another U and returning addition of both objects
end

local interface Multiplicable<U>
  mult : (U) => (U)
end

-- definition by trait combination
local typealias BaseArith<U> = Addable<U> & Multiplicable<U>

-- definition by cumbersome rewriting
local interface BaseArith<U>
  add : (U) => (U)
  mult : (U) => (U)
end
#+END_SRC

** Operator overloading

In lua, operator overloading is supported trough the metatable system and the metafields
 ~__add~, ~__sub~, ~__eq~, ... What is tricky is that theses metafields do not need to be
part of the actual type. But part of the type of the metatable.

We could propose a way to express in types that the metatable of the type must have some
particular fields. With the ~@~ symbol. The previous example would become :

#+CAPTION: proposed syntax for type traits union with operator overloading
#+BEGIN_SRC tl
local interface Addable<U>
  @__add : (U) => (U) -- method taking another U and returning addition of both objects
end

local interface Multiplicable<U>
  @__mul : (U) => (U)
end

-- definition by trait combination
local typealias BaseArith<U> = Addable<U> & Multiplicable<U>

-- definition by cumbersome rewriting
local interface BaseArith<U>
  @__add : (U) => (U)
  @__mul : (U) => (U)
end
#+END_SRC

* Presentation snippets

#+BEGIN_SRC lua
local function say_hello()
  print"Hello world"
end

say_hello()
#+END_SRC
