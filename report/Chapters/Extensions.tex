% Chapter Template

\externaldocument{Introduction}
\externaldocument{TypedLua}

\chapter{Extensions} % Main chapter title

\label{ch_extensions} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Initial goals}
\subsection{Environment change}

Lua has a peculiar way to handle global variables, when using names that are not reachable locals in scope,
the runtime tries to index the symbol \code{\_ENV} to find or depose the global values. This \code{\_ENV} is
in fact an upvalue of the current closure (lua scripts file are also functions). There exist a built-in
\code{setfenv} that allow to set a table that will serve as a global environment.

Alas, it appeared soon in the research process that this \code{setfenv} built-in was only available in Lua 5.1
which is two version behind the current 5.3 release. This was a good argument in favor of focusing on the
other initial idea. Typing the \code{coroutine} library.    

\subsection{Coroutine Library}

As explained in section \ref{subsec_coroutines}, at first glance, the coroutine library seem to be
typable if the type system supports genericity. After some research it appeared that someone
already managed to add generics support to Typed Lua as a Google Summer of Code project (\cite{clancy_2016}). But
this stayed as a fork and master implementation of TypedLua diverged quite a lot since. Moreover, this fork
implements a full class system for Typed Lua, which does not respect Lua semantics nor Typed Lua
structural sub-typing principles.

Since type checking the co-routines requires having generics merged into Typed Lua, decision has been
made to re-implement them by taking inspiration from Kevin Clancy's work, ignoring the nominal class
system. In addition, the implementation of local type parameters inference is added to reduce the
noise induced by generic type instantiation.

\section{Implementing Genericity}

\subsection{Generics representation}

A first step while implementing generics was to adapt the type system in \code{tltype.lua} 
to be able to express generic types that have types parameters. This \code{TGeneric} type
contains a list of type parameters along with their ids and a template-type that contains
some type variable (named \code{TParameter}) that matches the type parameters list. For example,
a generic function \code{<U>(U)->(U)} will result in this structure:

\begin{verbatim}
TGeneric(
  type_parameters={{"U", uid=0}},
  TFunction(
    TParameter("U", uid=0),
    TParameter("U", uid=0)
  )
)
\end{verbatim}

To generate an instance of such type it is only a matter of recursively replacing
the \code{TParameter}s by the corresponding concrete type. The uid is an unique 
identifier for the type parameter, allowing to handle shadowing of type variables.
The advantage of this parameter+template representation is that the template-type can be any type,
including a generic one.

\subsection{Functions}

The second step was to modify the grammar of Typed Lua to parse function type parameters.
The original Typed Lua grammar for functions looked like this :

\begin{align*}
\textit{stat} ::= & \;\; \texttt{...}\\
& | \; \textbf{local} \; \textbf{function} \; \textit{Name} \; \textit{funcbody}\\
& | \; \texttt{...}\\
\textit{funcbody} ::= & \;\; \texttt{`('} \; [\textit{parlist}] \; \texttt{`)'} \;
  [\texttt{`:'} \; \textit{rettype}] \; \textit{block} \; \textbf{end}\\
\textit{functiondef} ::= & \;\; \textbf{function} \; \textit{funcbody}\\
\end{align*}

And here it is modified to accept type parameters right before the function body.

\begin{align*}
\textit{stat} ::= & \;\; \texttt{...}\\
& | \; \textbf{local} \; \textbf{function} \; \textit{Name} \; \textit{funcbody}\\
& | \; \texttt{...}\\
\textit{typeidlist} ::= & \;\; \textit{Name} \; \{\texttt{`,'} \; \textit{Name}\} \\
\textit{funcbody} ::= & \;\; [\texttt{`<'} \; \textit{typeidlist} \; \texttt{`>'}] \; \texttt{`('} \; [\textit{parlist}] \; \texttt{`)'} \;
  [\texttt{`:'} \; \textit{rettype}] \; \textit{block} \; \textbf{end}\\
\textit{functiondef} ::= & \;\; \textbf{function} \; \textit{funcbody}\\
\end{align*}

The AST construction routines have then been modified so that function definitions
nodes carry their type parameters. Those type parameters fields will then be 
used in the checking phase to resolve the type of the functions.

\subsection{Interfaces}

Typed Lua interfaces are type aliases to record types. A second \code{typealias}
keyword exist to declare names for type that are not only tables. Of course these
names are only shorthand to the actual structural type that they define, two
really unrelated names can describe similar types.

The grammar transformations are the following :

before
\begin{align*}
\textit{stat} ::= & \;\; \texttt{...}\\
& | \; [\textbf{local}] \; \textbf{interface} \; \textit{typedec}\\
& | \; [\textbf{local}] \; \textbf{typealias} \; \textit{Name} \; \texttt{`='} \; \textit{type}\\
& | \; \texttt{...}\\
\textit{typedec} ::= & \;\; \textit{Name} \; \{\textit{decitem}\} \; \textbf{end}\\
\end{align*}

after
\begin{align*}
\textit{stat} ::= & \;\; \texttt{...}\\
& | \; [\textbf{local}] \; \textbf{interface} \; \textit{typedec}\\
& | \; [\textbf{local}] \; \textbf{typealias} \; [\texttt{`<'} \; \textit{typeidlist} \; \texttt{`>'}] \; \textit{Name} \; \texttt{`='} \; \textit{type}\\
& | \; \texttt{...}\\
\textit{typedec} ::= & \;\; [\texttt{`<'} \; \textit{typeidlist} \; \texttt{`>'}] \; \textit{Name} \; \{\textit{decitem}\} \; \textbf{end}\\
\end{align*}

In this case, AST node construction directly does the transformation to a \code{TGeneric}
node if the \code{typealias} or \code{interface} has one or more type parameters.

\subsection{Usage}

The \code{tltype} modules is able to represent the generic types, the \code{tlparser} and
\code{tlast} modules are updated to produce and carry those details in the AST. The one thing
that is missing is ways to apply those generic types. For this, type arguments need
to be part of the grammar.

before :

\begin{align*}
\textit{var} ::= & \;\; \textit{Name} \; | \;
  \textit{prefixexp} \; \texttt{`['} \; \textit{exp} \; \texttt{`]'} \; | \;
  \textit{prefixexp} \; \texttt{`.'} \; \textit{Name}\\
\textit{prefixexp} ::= & \;\; \textit{var} \; | \;
  \textit{functioncall} \; | \;
  \texttt{`('} \; \textit{exp} \; \texttt{`)'}\\
\textit{functioncall} ::= & \;\; \textit{prefixexp} \; \textit{args} \; | \;
  \textit{prefixexp} \; \texttt{`:'} \; \textit{Name} \; \textit{args}\\
\textit{args} ::= & \;\; \texttt{`('} \; [\textit{explist}] \; \texttt{`)'} \; | \;
  \textit{tableconstructor} \; | \;
  \textit{String}\\
\textit{primarytype} ::= & \;\; \textit{literaltype} \; | \;
  \textit{...} \; | \;
  \textit{Name}\\
\end{align*}

after :

\begin{align*}
\textit{typeargs} ::= & \;\; \texttt{`<'} \; \textit{type} \; \{\texttt{`,'} \; \textit{type}\} \; \texttt{`>'}\\
\textit{var} ::= & \;\; \textit{Name} \; [\textit{typeargs}] \; | \;
  \textit{prefixexp} \; \texttt{`['} \; \textit{exp} \; \texttt{`]'} \; | \;
  \textit{prefixexp} \; \texttt{`.'} \; \textit{Name} \; [\textit{typeargs}]\\
\textit{prefixexp} ::= & \;\; \textit{var} \; | \;
  \textit{functioncall} \; | \;
  \texttt{`('} \; \textit{exp} \; \texttt{`)'}\\
\textit{functioncall} ::= & \;\; \textit{prefixexp} \; \textit{args} \; | \;
  \textit{prefixexp} \; \texttt{`:'} \; \textit{Name} \; \textit{args}\\
\textit{primarytype} ::= & \;\; \textit{literaltype} \; | \;
  \textit{...} \; | \;
  \textit{Name} \; [\textit{typeargs}]\\
\end{align*}

As always, \code{tltype} and \code{tlast} modules have been updated to reflect those
changes. Everything is set up so that \code{tlchecker} can check the generics constructions.

\subsection{Checking}

Checking needs to be updated for specific cases : generic functions definition,
generic function application, and specialization of generic type aliases.

\paragraph*{Function definition}
The type parameters that are declared in this extension do not accept type
bounds, although this would not be a difficult task to add them. When handling a
function definition, the code looks if it accepts type parameters. In this case,
 it declares a new scope using the \code{tlst} environment module. In this
new scope that encloses the function definition, it declares types aliases for each
type parameters. The concrete type of those type alias is a \code{TParameter}, holding
the \code{uid} generated for this scope. This permits to treat the references having
this type as black boxes.

Sub-typing rules are updated accordingly so that \code{TParameter}s are only
sub-types of themselves. In particular, the \code{uid} is checked instead of
the actual name of the variable to treat correctly cases where multiple variables
have the same name, (this happens only in rare case with nested functions).

The function is then checked like any function, the declared type variables
and the new sub-typing rules take care of propagating the \code{TParameter}
until the return type is deduced. Note that in Typed Lua return types are
optional annotations, the checker just verify that inferred return type is
coherent with the declared one, if any.

\paragraph*{Generic function application}

Using type arguments introduced in the grammar in the preceding section permits 
to instantiate the generic functions.

As a first step, the checker needs to ensure that type arguments are used only
on \code{TGeneric} types. Then it can substitute the \code{TParamters} of
the type template with the concrete type arguments. This is done using a dedicated
routine in the \code{tltype} module. It then can replace the type of the expression
by the newly instanced one.

\paragraph*{Generic type aliases}

The type variables that refer to generic interfaces or type aliases are
processed similarly. When type variable are substituted with their
concrete type, if the type is generic and type arguments where provided,
the type is instantiated.

\section{Type parameters inference}
\subsection{Motivation}

Although the current state of the implementation is already capable of
manipulating generic types. The programmer is asked to type a lot of
redundant information that can be considered as noise. An example:

\begin{verbatim}
local function make_pair<A,B>(a : A, b : B)
      return {first=a, second = b}
end

local my_pair = make_pair<string, number>("one", 1)
\end{verbatim}

Could be less verbose without any sacrifice in clarity :

\begin{verbatim}
local function make_pair<A,B>(a : A, b : B)
      return {first=a, second = b}
end

local my_pair = make_pair("one", 1)
\end{verbatim}

This does not make much difference in this case, but can greatly help
when type argument are far less trivial types as this can be the case with
structural type systems.

\subsection{Implementation}

In preamble, it is necessary to detect when type parameters inference is
necessary. Since the the variables and prefixed expressions produced by function
definition in the previous sections have type \code{TGeneric} on purpose,  it
is an easy task. The type checker looks for cases where the programmer calls
\code{TGeneric} variables or expression without giving type arguments. In this
case it has two type tuples : one from the template type and one from the given
type arguments.

The problem is now to deduce (infer) type parameters from those two type tuples.

\subsection{Algorithm}

\subsubsection{Naive version}

When having to solve this problem, a primary reflex was to come up with the
simplest algorithm possible. This algorithm did not take variance into account and
failed in quite simple cases.

Although this first version of the algorithm is obviously unsound. It resemble a
lot the \emph{unification} algorithm \cite{pierce2002types}. But in this case
it does not unify set of constraints to deduce a substitution but directly two
type structures. There is also a huge difference in scale. While \emph{unification}
is meant to infer types for a whole program. The algorithm we need can be far less
powerful as it only need to do local type inference.

\newpage

\begin{verbatim}
# P : parametric type template
# G : given type
# S : substitution from TypeParameters to types
# R : <: or :>, subtyping direction
infer(P, G, S, R) =
  if P = G
    then S
  else if P is TParam X and X not in S
    then S o [X -> G]
  else if P is TParam X and X in S
    then let Tx be S(X) in
      if G R Tx
        then S
      else if Tx relation G #
        then S o [X -> given] # update substitution to broader type
      else
        fail # could not concile given type
  else if P = P1->P2 and G = G1->G2 # functions
    then let S1 = infer(P1, G1, S, ~ R) in
      infer(P2, G2, S1, R)
  else if P = P1*...*PN and G = G1*...*GN # tuples
    then foldLeft(zip(P,G), S,
                  (Sn, (Pn, Gn)) => infer(Pn, Gn, Sn, R))
  else if P = P1+...+PN and G = G1+...+GN # unions
    then foldLeft(zip(P,G), S,
                  (Sn, (Pn, Gn)) => infer(Pn, Gn, Sn, R))
  else if P = {P1:P2?} and G = {G1:G2?} # maps
    then let S1 = infer(P1, G1, S, R) in
      infer(P2, G2, S, R)
  # records
  else if P = {KP1:VP1,...,KPn:VPn} and G = {KG1:VG1,...,KGn:VGn}
     then infer each K:V pair recursively
  else
    fail
\end{verbatim}

As one can see, the variance information was contained in the variable \code{R},
but the substitution \code{S} itself does not contain any context that would 
indicate in which variance relation it was emitted.

Thankfully, there is plenty of literature about type inference, and after some
readings, especially \cite{pierce2000local}, it was easier to avoid those
errors.

\subsubsection{Sounder version}

\newpage
\begin{verbatim}
# P : parametric type template
# G : given type
# B : substitution from TypeParameters to type bounds, 
# start with S = [Nothing <: X <: Value] for all X
# R : <: or :> or invariant, subtyping relation,
infer_bounds(P, G, B, R) =
  if P = G
    then S
  else if P is TParam X
    then let [S <: X <: T] be B(X) in
      if R is <:
        then if G <: S
          then B
        else if S <: G
          then B o [G <: X <: T]
        else
          fail
      if R is :>
        then if G :> T
          then B
        else if T :> G
          then B o [S <: X <: G]
        else
          fail
      if R is invariant
        then if S <: G <: T
          then B o [G <: X <: G]
        else
          fail
  else if P = P1->P2 and G = G1->G2 # functions
    then let B1 = infer_bounds(P1, G1, B, ~ R) in
      infer_bounds(P2, G2, B1, R)
  else if P = P1*...*PN and G = G1*...*GN # tuples
    then foldLeft(zip(P,G), B,
                  (Bn, (Pn, Gn)) => infer_bounds(Pn, Gn, Bn, R))
  else if P = P1+...+PN and G = G1+...+GN # unions
    then foldLeft(zip(P,G), B,
                  (Bn, (Pn, Gn)) => infer_bounds(Pn, Gn, Bn, R))
  else if P = {P1:P2?} and G = {G1:G2?} # maps
    then let B1 = infer_bounds(P1, G1, B, R) in
      infer_bounds(P2, G2, B1, R)
  # records
  else if P = {KP1:VP1,...,KPn:VPn} and G = {KG1:VG1,...,KGn:VGn}
     then infer each K:V pair recursively, key should be invariant...
  else
    fail
\end{verbatim}

However, the result of running this algorithm is a mapping from type parameter to
types bounds. There is a second pass that must choose which bound is the
tightest type for a given type parameter.

\begin{verbatim}
for each [S <: X <: T] in B
  assert(S <: T)
  if P is covariant or constant in X:
    then X -> S
  else if P is contravariant in X:
    then X -> T
  else if P is invariant in X and S=T:
    then X -> S
  else
    fail
\end{verbatim}

\subsection{Lowest upper bounds}

There is still cases in which the inference will fail although a solution exists.
For example:

\begin{verbatim}
local function pair<U>(a : U, b : U) return a, b end

pair({x = 1, y = 2}, {x = 3, z = 'a'}) -- U could be {x : integer}
\end{verbatim}

For records, one can intuitively see that the answer is an intersection of 
the keys of the record. In a nominal type system, it would just be a matter
of visiting the inheritance graph for a youngest common ancestor. But a structural
sub-typing setting, this become more complex. The following helper function does not aim
to cover all cases but offer already more opportunities to infer compatibles but unrelated
types:

\begin{align*}
lub(a, b) \; \texttt{if} \; a <: b = & \; b\\
lub(a, b) \; \texttt{if} \; b <: a = & \; a\\
lub(a <: integer, b <: integer) = & \; integer\\
lub(a <: number, b <:number) = & \; number\\
lub(a <: string, b <: string) = & \; string\\
lub(a : record, b : record) = & \; a \cap b\\
lub(a : union, b : union) = & \; a \cup b\\
\end{align*}

Special care must be taken for records keys that are literals as they play a crucial
role in the type system, they indicate named fields of the "objects".

\section{Coroutines}

Coroutines were the initial goal of this project, the question is now : does the improved
type system allows to type them?

As expressed previously in \ref{subsec_coroutines}, typing the library directly would imply
to add a language-level feature to track closure execution and deduce if current function
is ran in a coroutine or not. This kind of intricate analysis is out of scope but there is
a more obvious path : write a safe wrapper around the unsafe coroutine library.

The interface to such a wrapper should reflect as much as possible the actual behavior of
the standard library but avoid the use of free functions whose behavior depend on the context.
Here is a proposition:

\begin{verbatim}
-- helper definitions
interface CoroHandle<I,O>
   resume : (I) -> (boolean, O)
   status : () -> (string)
end

interface CoroThread<I,O>
   yield : (O) -> (I)
end

typealias CoroFun<I,O> = (CoroThread<I,O>, I) -> (O)

-- module interface
interface SafeCouroutine
  create : <I,O>(CoroFun<I,O>) -> (CoroHandle<I,O>)
  wrap   : <I,O>(CoroFun<I,O>) -> ((I)->(O))
end
\end{verbatim}

By giving a \code{CoroThread} object as first argument to the function, we ensure that only
a function declared with coroutines in mind can be passed as argument to the \code{create}
function, and that the function can only yield values consistent with its declared types. The
main thread caller receives a \code{CoroHandle} that offers the two functions that an owning
thread was expected to call. The actual \code{thread} user-data object provided by the standard
library is hidden  as an implementation detail, the user has no choice but to use type safe
methods to manipulate the coroutine. On the coroutine side only the yield function is available
while on the caller side only the resume and status function are exposed. This mechanically
separates the two contexts without relying on some language level feature.

One could argue that the possibility to yield multiple values from the coroutine has been
lost, and that is true. This could be fixed by adding parameters packs to the type parameters,
like C++ does, but this is out of scope. The user can always wrap his several values into a
table, which does not change the syntax much :

\begin{verbatim}
-- with the standard library :
coroutine.yield(a, b, c, d)

-- with our solution
tread.yield{a, b, c, d}
\end{verbatim}

This uses another syntactic sugar of Lua that allows to omit parentheses if the single argument
of the function is a table.

Here is an example of the use of the library :

\begin{verbatim}
local safe_coroutine = require'examples.gcoro_lib'

--------------------------------------------------------------------
-- first simple example as a producer, consumer, almost function-like
--------------------------------------------------------------------
local function check_string(n : string) print(n) end

local chandle = safe_coroutine.create(
   function(thread : CoroThread<number,string>, n : number) : string
       while true do
          n = thread.yield(tostring(n))
       end
       return tostring(n)
   end
)

for i = 1,10 do
    local _, n = chandle.resume(i)
    check_string(n)
end
\end{verbatim}

Source code of the library can be found in appendix \ref{CoroSource}
\chapter{Further tests}

This chapter describes some tests that were conducted aside the main task of having a
working typed coroutine library. Each section describe a particular untyped construct
of Typed Lua and explore the way it could be typed. Some of theses side researches made it to
the final implementation.

\section{Operator overload}
Lua supports some form of operator overloading, trough the meta-table mechanism. If a
table have a meta-table attached, Lua's run-time will look at some fields in that meta-
table that correspond to each operators :

\begin{verbatim}
__add      a + b
__sub      a - b
__mul      a * b
__div      a / b
__mod      a % b
__pow      a ^ b
__unm      -a
__concat   a .. b
__len      #a
__eq       a == b
__lt       a < b
__le       a <= b
__index    a[k]
__newindex a[k] = v
__call     a(args...)
\end{verbatim}

What makes operator overloading difficult is that it does not depend on fields of the
current table but on fields of the meta-table. 

\subsection{Meta-fields}

Typed Lua already has \emph{const} fields to allow sub-typing between object types. The
proposition is to add \emph{meta} fields that will acknowledge the presence of the field
in the meta-table attached to the table that carry the type. This would be written as
follow :

\begin{verbatim}
interface Vec2
  @__add : (Vec2, Vec2) -> (Vec2)
  @__umn : (Vec2) -> (Vec2)
  @__mul : (Vec2, Vec2) -> (number)
end
\end{verbatim}

Implementing this is not much more different than the generic functions and interfaces.
So it won't be detailed too much. Still, an important change was committed to the subtyping
rules to separate meta-fields from normal fields. In addition, the \code{tltype} routine used to get
field from table was separated in two versions : one that gets regular fields and an other
getting only meta-fields. This helps the \code{tlchecker} module to find meta-fields when
operators are used on table types.

So far, the compiler can handle references with meta-field and deduce the type of expressions
involving table that contains the right meta-fields but there is no way to construct those
types. In Typed Lua, one could hide the untyped construction of the values that carry those
types behind a \code{.tld} file, even though this is somehow not satisfying. In plain Lua, this is
usually done using \code{setmetatable} but as noted earlier (\ref{subsec_setmetatable}),
Typed Lua restrains itself to very specific use of this built-in function.

\subsection{Typing \code{setmetatable}}

\begin{verbatim}
setmetatable(table, metatable) -- returns table
\end{verbatim}

As one can see, the table passed as first argument is returned back. In fact, the function
can be used without getting the return value since it has the side effect of attaching the
metatable. However, this attempt will only consider cases where the first argument to the
function is a \code{unique} table. This is already what Typed Lua does and this allows to
ignore the side effects of the function.

To compute the resulting type, the two record types are merged together. Fields of the
meta-table become meta-fields and the ones from the table are just copied. Choice is made
to ignore the effect of the \code{\_\_index} and \code{\_\_newindex} for the moment. These
fields control two reflexive features that are likely to break any assumption that Typed
Lua makes about the type of the tables. Current Typed Lua implementation only handle the
case where metatable is \code{\{\_\_index=self\}}. This attempt to accept operator overloading
preserves this special case and otherwise produce the merged type described earlier.

An example:

\begin{verbatim}
local interface BoxedNumber
   val : number
   @__add : (BoxedNumber, BoxedNumber) -> (BoxedNumber)
end

-- hacky forward declaration
local BoxedNumber : any

-- metatable construction
local mt = {}

function mt.__add(r : BoxedNumber, l : BoxedNumber)
   return BoxedNumber(r.val+l.val)
end

-- value constructor
function BoxedNumber(n : number)
   return setmetatable({val=n}, mt)
end

local one = BoxedNumber(1)
local two = BoxedNumber(2)

local three = one + two
\end{verbatim}

Alas, there is no other way than to forward declare the constructor with a dynamic type.
To do better and fully type this example we would need a way to correctly forward declare
functions. Currently, Typed Lua gives an optional type to forward declared values that are
captured in closures.

\begin{verbatim}
-- less-hacky forward declaration
local BoxedNumber : (number) -> (BoxedNumber)

function mt.__add(r : BoxedNumber, l : BoxedNumber)
   return BoxedNumber(r.val+l.val) -- error :
   -- attempt to call BoxedNumber of type "(number) -> (BoxedNumber)?"
end
\end{verbatim}

That is somehow related to how Typed Lua checks all the definitions in order. This respect
Lua semantics, definitions are statements and can be mixed with control flow and function 
calls that could have side effects or try to call the forward declared value

