% Chapter 1

\chapter{Introduction}

\section{Script languages}

Dynamically typed language like Lua, JavaScript, Python, Ruby are often referred to as
\emph{script languages}. Although their syntax and run-time semantics widely differ, they
have in common the absence of static type annotations. For these languages that target
maximum flexibility and don't request the programmer specify the types of the values
they manipulate, type checking happens right during the execution. What this reflects is
that theses script languages are indeed typed, some are even strongly typed. But type
checking happen later, in fact, at the latest possible time. Runtimes of languages like
Lua use type tags to attach types to values during execution and discover the type of 
the newly produced references and variables as the program advance. While this relieve
some pressure on the programmer side, this advantage and expressiveness can become a curse
as scripts or applications grow into larger software. If, at first, the coder's mind is
lightened by the absence of types, connecting bigger and bigger modules with untyped frontend
together can become a challenge. As well as developing complex data structures or intricate
data processing pipelines can produce cognitive surcharge as the mind can not hold the
entire picture at once and fails to reason about the soundness of the written code.

\section{Optional types}

Because migrating dynamically typed code to a new statically typed language can be cumbersome,
not only because the change in paradigm but also because these two kind of languages tend
not to share the same concepts and syntax, the programmer should have the option to use
types in his scripting language. One could argue that languages must be used for the purposes
they were designed for. But there is a vast variety of application and some need mainly
to treat dynamic information with dynamic types and, at some point, have a tiny module
that would benefit a static type system to ease design and debugging. For long, Lua's 
solution to this problem was to allow to call routines written in other languages (mainly C).
This helped a lot on the performance side of course, but it was costly to move the barrier
between the two languages, as binding boilerplate has to be written to connect both worlds.
The need for optional types annotations comes from a desire of correctness, not for performance.
Many scripts languages got their own typed equivalent, often in the form of transpilers
(Typescript, Typed Lua) or Language features (Python 3) with more-less success.
This project builds extensions on top of Typed Lua to allow more Lua Idioms to be typed.
This report explains the need for new features and their implementations, along with the challenge
that modifying Typed Lua code base represent.

\chapter{Lua} % Main chapter title

\label{ch_lua} % For referencing the chapter elsewhere, use \ref{Chapter1} 

%----------------------------------------------------------------------------------------

% Define some commands to keep the formatting separated from the content 
\newcommand{\keyword}[1]{\textbf{#1}}
\newcommand{\tabhead}[1]{\textbf{#1}}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\file}[1]{\texttt{\bfseries#1}}
\newcommand{\option}[1]{\texttt{\itshape#1}}

%----------------------------------------------------------------------------------------

\section{Introduction}

Lua is an imperative scripting language that feature a very clean syntax and
very little syntactic sugar. It was designed with extensibility in mind, not only
from Lua but also from other languages like C or C++.

\section{Language Constructs}

Lua's philosophy is to limit the amount of language features and concepts to the smallest
possible set allowing users to write their own if they need them. This helps Lua being
one of the smallest scripts languages available, with very small footprint in code size
and run-time memory. Its simplicity also permits to port it to a vast amount of platforms
and hardware of diverse capabilities.

\subsection{Functions}

Beside its indispensable primitive types that are \code{string} and \code{number},
Lua features first class functions, which allow to use it as a functional language.
There are some important particularities to Lua function's call.
First, the arity is
not checked. This means that one can call a function with more or less arguments than
declared with, superfluous arguments are dropped while missing ones are replaced
by \code{nil}. 
Second, functions can have multiple return values and, similar to function call, 
variables assignement to a function's result will drop or replace by nil to match the
destination variables count.

\subsection{Tables}

Tables are the only collection type of Lua. They are mutable records that can have any
Lua object as key, except nil. Tables themselves, while really useful, does not permit
a great variety of behavior. However tables can already behave like "objects" in the 
Object-Oriented Programming sense thanks to one of the only syntactic sugars Lua offers :

\code{ my\_table:my\_method(args...) }

This indexes the table with \code{my\_method} and calls the value with the table as
first argument (the \code{self} argument). This syntax differs from a regular function
call :

\code{my\_table.my\_function(args...) }

\subsection{Meta-Tables}

Meta-tables are the most powerful concept of Lua. Since the language has only tables,
to change the behavior of those, you can attach to them a \code{metatable} with the
\code{setmetatable} built-in. Lua run-time will look at a variety of \emph{meta-fields}
in the meta-table that will indicate how indexing or manipulating the regular table
will behave. A subset of the possibilities :

\begin{itemize}
\item Operator overloading
\item Read-only fields
\item Weak references to help garbage collection
\item Inheritance
\end{itemize}

For more information see \citep{ierusalimschy2006programming}.

\section{Runtimes}

One argument in favor of Lua is also the remarkable performance of its implementations.
The regular Lua runtime, written in plain ANSI C already performs decently. And when
scripting speed is a concern, \code{luajit} can lead to impressive speed-up of up to a
hundred times.

\subsection{Embedding Lua}

As said earlier, Lua was designed with simplicity in mind and extensibility. Interacting
with the stack-machine that runs Lua is straightforward thanks to a clean C API and both
run-times can be embedded in a compiled program that can easily expose new libraries and
functionality to the Lua scripts it runs.