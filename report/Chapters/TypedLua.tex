% Chapter Template

\externaldocument{Introduction}
\externaldocument{Extensions}

\chapter{Typed Lua} % Main chapter title

\label{ch_typed_lua} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}


%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

Typed Lua is a project born in the same university where Lua was designed : the
\emph{Pontifical Catholic University of Rio de Janeiro} in Brazil. The project was
supervised by Prof. Roberto \textsc{lerusalimschy}, one of the initial Lua designers, and
led to two publications (\cite{maidl2014typed} and \cite{maidl2015formalization}) and a doctoral thesis (\cite{maidl2015thesis})
. This means that TypedLua was developed in an academic
environment and therefore the respect of the Lua idioms and concepts was a central concern.

\section{Optional type system}

Typed Lua is an optional type system for Lua. It is meant to progressively add
typing annotations in existing Lua code-bases for example. As explained in the related
thesis, it differs from Gradual Typing where run-time checks are added to ensure
consistency between statically typed and dynamically typed regions. Here, the dynamic type
\code{any} is used.

\subsection{Consistent subtyping}

The type \code{any} is subject to \emph{consistent subtyping}, that means that it is
both subtype and supertype of any type. However, this is different from the usual
subtyping relation : consistent subtyping is not transitive. This means, that \code{any}
is not a top nor bottom type in Typed Lua type system. It represents, as its names suggests,
any type.

\section{Type system}

\begin{figure}[!h]
\textbf{Types}\\
%\dstart
\begin{align*}
\textit{type} ::= & \;\; \textit{primarytype} \; [\texttt{`?'}]\\
\textit{primarytype} ::= & \;\; \textit{literaltype} \; | \;
  \textit{basetype} \; | \;
  \textbf{nil} \; | \;
  \textbf{value} \; | \;
  \textbf{any} \; | \;
  \textbf{self} \; | \;
  \textit{Name}\\
& | \; \textit{functiontype} \; | \;
  \textit{tabletype} \; | \;
  \textit{primarytype} \; \texttt{`|'} \; \textit{primarytype}\\
\textit{literaltype} ::= & \;\; \textbf{false} \; | \;
  \textbf{true} \; | \;
  \textit{Int} \; | \;
  \textit{Float} \; | \;
  \textit{String}\\
\textit{basetype} ::= & \;\; \textbf{boolean} \; | \;
  \textbf{integer} \; | \;
  \textbf{number} \; | \;
  \textbf{string}\\
\textit{functiontype} ::= & \;\; \textit{tupletype} \; \texttt{`->'} \; \textit{rettype}\\
\textit{tupletype} ::= & \;\; \texttt{`('} \; [typelist] \; \texttt{`)'}\\
\textit{typelist} ::= & \;\; \textit{type} \; \{\texttt{`,'} \; \textit{type}\} \; [\texttt{`*'}]\\
\textit{rettype} ::= & \;\; \textit{type} \; | \;
  \textit{uniontuple} \; [\texttt{`?'}]\\
\textit{uniontuple} ::= & \;\; \textit{tupletype} \; | \;
  \textit{uniontuple} \; \texttt{`|'} \; \textit{uniontuple}\\
\textit{tabletype} ::= & \;\; \texttt{`\{'} \; [\textit{tabletypebody}] \; \texttt{`\}'}\\
\textit{tabletypebody} ::= & \;\; \textit{maptype} \; | \;
  \textit{recordtype}\\
\textit{maptype} ::= & \;\; [\textit{keytype} \; \texttt{`:'}] \; \textit{type}\\
\textit{keytype} ::= & \;\; \textit{basetype} \; | \;
  \textbf{value}\\
\textit{recordtype} ::= & \;\; \textit{recordfield} \; \{\texttt{`,'} \; \textit{recordfield}\} \; [\texttt{`,'} \; \textit{type}]\\
\textit{recordfield} ::= & \;\; [\textbf{const}] \; \textit{literaltype} \; \texttt{`:'} \; \textit{type}
\end{align*}
%\dend
\caption{The concrete syntax of Typed Lua types}
\label{fig:types}
\end{figure}

Typed Lua type system is straightforward for the primitive types \code{string}, \code{number},
\code{integer} and \code{boolean}. However, literals have separated types, along with
subtyping relations, to allow expressing fields of tables with types. A short example:

\code{local t = \{a = "str", b = 1, [0]="test"\}}

will have type :

\code{\{"a" : "str", "b" : 1, 0 : "test"\}}

implemented like this :

\begin{verbatim}
TTable(
    TField(TLiteral("a"), TLiteral("str")),
    TField(TLiteral("b"), TLiteral(1)),
    TField(TLiteral(0), TLiteral("test"))
)
\end{verbatim}

A complete syntax of the types of Typed Lua is in figure \ref{fig:types}. One important thing
to consider is that Typed Lua, trying to respect Lua concepts as much as possible, has only
structural subtyping.

\subsection{Table refinement}

Since tables are the only way to construct objects in Lua, special care is taken to
type them as they are constructed. As André Murbach \textsc{Maidl} reported in \cite{maidl2015formalization},
26\% of table constructions in his examined corpus where empty constructors.
Table filling is usually incremental and require the type of the table to be updated, this
reflects the imperative nature of Lua.

To encode this, table types are attached a tag indicating if the table is \code{unique},
\code{open}, \code{closed} or \code{fixed}. Each of these tag allow a different sets of
operations to be performed on the table variables. The following sections give details about
the behavior implied by those tags.

\subsubsection{\code{unique}}

\begin{quote}
A table type which guarantees that there are no keys with a type that is not one of its 
key types, and that has only one reference.
\end{quote}

This is usually the type of tables right after they are constructed. References to tables
of this type are allowed to incrementally change type. Since there are guaranteed to be the
unique references.

\subsubsection{\code{closed}}

\begin{quote}
A table type that does not provide any guarantee about keys with types not listed in the table type.
\end{quote}

Closed tables are the most common table type. This is the type given to annotated variables, interfaces
and userdata. This tag only guarantees is that the table indeed has the key-value pair listed in its
type, but nothing more. Closed reference to tables cannot incrementally add entries into them.

\subsubsection{\code{open}}

\begin{quote}
A table type which guarantees that there are no keys with a type that is not one of its key types,
and that only have closed references.
\end{quote}


Only having closed references allow an open table reference to be used to add keys to the table.
This is usually the type of reference to table that have been captured as upvalues:

\begin{verbatim}
local t = {text= "hello world"} -- t is a unique reference

local function say_hello()
  -- t is a closed reference with type {"text":"hello world"}
  print(t.text)
end

-- t is an open reference, so it can be modified
t.say_hello = say_hello 

local function use_t()
  -- t is closed reference with type
  -- {"text":"hello world", "say_hello":()->()}
  t.say_hello()
end
\end{verbatim}

\subsubsection{\code{fixed}}

\begin{quote}
A table type which guarantees that there are no keys with a type that is not one of its
key types, and that can have any number or \code{fixed} or \code{closed} references.
\end{quote}

Multiple references with guarantees about their width have this tag. This sealed type is 
used for class prototypes in the current object oriented programming way of using 
Typed Lua. 

\subsection{Flow typing}

Typed Lua implements flow typing to allow several Lua idioms to type check correctly.
It deduces finer types for variables in branches or results of logical operations.

A short example:

\begin{verbatim}
local function say_hello(name : string?)
  local name = name or "Sir" --new local "name" shadows
  -- parameter "name" and is no more an Option<string>
  
  print("Hello ", name)
end  
\end{verbatim}

Here flow typing is only used to refine the result of the \code{or} operation.
But it can also discriminate union of primitive types :

\begin{verbatim}
local function add_or_concat(a : string | number,
                             b : string | number)
  if type(a) == "string" and type(b) == "string" then
    -- a and b have type string here
  	return a .. b -- return concatenation of argument
  elseif type(a) == "number" and type(b) == "number" then
    -- a and b have type number here    
    return a + b
  else
    error("incompatible types")
  end
end
\end{verbatim}

Note that in this case the return type of the function will deduce to \code{string | number}
which is not ideal but is a limitation of Typed Lua that does not support overload on the arguments
nor on the return type.

But flow typing does not stop here, as promised in \cite{maidl2015thesis},
it can also discriminate union of tagged tables:

\begin{verbatim}
local typealias Exp = {"tag":"Number", 1:number}
                    | {"tag":"Add", 1:Exp, 2:Exp}
                    | {"tag":"Sub", 1:Exp, 2:Exp}
                    | {"tag":"Mul", 1:Exp, 2:Exp}
                    | {"tag":"Div", 1:Exp, 2:Exp}

local function eval (e:Exp):number
  if e.tag == "Number" then return e[1]
  elseif e.tag == "Add" then return eval(e[1]) + eval(e[2])
  elseif e.tag == "Sub" then return eval(e[1]) - eval(e[2])
  elseif e.tag == "Mul" then return eval(e[1]) * eval(e[2])
  elseif e.tag == "Div" then return eval(e[1]) / eval(e[2])
  end
end
\end{verbatim}

However, current implementation available on \emph{github} seem to be broken at this level. Notice that
Exp is a recursive type here. Recursive types are allowed to be defined in \code{interface} and \code{typealias} statements.



\section{Untyped constructs}

A lot of effort was put into making Typed Lua reflect as much as possible
the semantics and idioms of typical Lua code. Its structural sub-typing, table
refinement and flow typing do really allow to write Lua code with some type
annotations and have the transpiler verify them, raise error when
they are incompatible or warn you when you cross the dynamic V.S. static typing
 barrier.

Beside all this, Typed Lua fails when it comes to higher order functions and
libraries that would require genericity.

Lua has first class functions and, altough the absence of syntactic sugar to define
them, also has anonymous functions. There are a handful of libraries that offer collections
or iterator manipulation routines. Those can't be typed given the lack of genericity
in the current implementation. But third party libraries are not the only ones to suffer
from this, a really useful branch of the standard library is also un-typable : \emph{coroutines}. 

\subsection{Coroutines} \label{subsec_coroutines}

The standard \emph{coroutine} library allows Lua programmers to write asynchronous code
using collaborative multi-tasking. This library allow to spawn threads of execution and to yield
values from one to the other, resuming execution on one or the other side. 

For the reader to get a better grasp of the use of the coroutine library, here is 
an example demonstrating some value passing from one thread to the other, along with its
output.

\begin{verbatim}
local coro = coroutine

local function coro_test(a,b,c)
  print(a, b, c)
  local g, h, i = coro.yield('d','e','f')
  print(g, h, i)
  return 'j', 'k', 'l'
end

local thread = coro.create(coro_test)

local s,d,e,f = coro.resume(thread, 'a', 'b', 'c')
print(s,d,e,f)
local s,d,e,f = coro.resume(thread, 'g', 'h', 'i')
print(s,a,b,c)
local s = coro.resume(thread)
print(s)
\end{verbatim}

\begin{verbatim}
#Output
a	b	c
true	d	e	f
g	h	i
true	nil	nil	nil
false
\end{verbatim}

Here is the current interface of the \emph{coroutine} library from Typed Lua description files:

\begin{verbatim}
userdata thread end

create : ((any*) -> (any*)) -> (thread)
isyieldable : () -> (boolean)
resume : (thread, any*) -> (boolean, string|any, any*)
running : () -> (thread, boolean)
status : (thread) -> (string)
wrap : ((any*) -> (boolean, string|any, any*)) ->
       ((any*) -> (boolean, string|any, any*))
yield : (any*) -> (any*)
\end{verbatim}

One can see that almost all symbols make extensive use of the dynamic type \code{any},
but the types returned and expected by those functions are almost all deterministic and
could easily be deduced. Here is a summary of those type relations written with type
variables:

\begin{verbatim}
userdata thread[P,R] end

-- P = (P1, P2, ...)
-- R = (R1, R2, ...)

-- allocates a new thread that will run the given function
create : ((P1,P2, ...) -> (R1,R2, ...)) -> (thread[P,R])

-- start or resume a thread execution, giving it some values,
-- values yielded from the thread are returned
resume : (thread[P,R], P1, P2, ...) -> (boolean, string|R1, R2, ...)

-- gets the running thread or nil if called on the main thread
running : () -> (thread[P,R]?)

-- gets the status of a thread
status : (thread[P,R]) -> (string)

-- wrap a coroutine created with given function in another
-- function with the same parameters
wrap : ((P1, P2, ...) -> (boolean, string|R1, R2, ...))
    -> ((P1, P2, ...) -> (boolean, string|R1, R2, ...))

-- suspend current thread and give values to the calling `resume`
-- call, next resume call values are returned 
yield : (R1, R2, R3) -> (P1, P2, ...)
\end{verbatim}

Of course some of these types are still coming from the execution context, making it difficult
to only encode this using a type system. Some language level support would be required to check
this correctly.

\subsection{setmetatable} \label{subsec_setmetatable}

As described in \ref{ch_lua}, the \code{setmetatable} function is a corner-stone of the Lua language.
It is used to implement all object oriented features, it is so powerful that it can even handle
multiple inheritance.

Typed Lua rules for \code{setmetatable} have been designed to handle a very specific case that permits
prototype based class systems and single inheritance. This restrictive usage of meta-tables ignores
very useful features such as operator overloading.

\subsection{Mutual Recursion}

Imperative nature of Lua and lack of declarative scope makes it difficult to handle mutual recursion.
Mutual recursion between methods comes for free in Lua because it indexes the table with the method name
and can thus always find it. Mutual recursion between local functions can be achieved using forward
declarations.

\begin{verbatim}
local g -- forward declare G
local function f()
  return g()
end

function g() -- assign a concrete implementation to g
  return f()
end
\end{verbatim}

Which is difficult to type, as definitions could be mixed with control-flow. 

\subsection{About Typed Lua implementation}

This section is a small interlude giving an overview about the way Typed Lua is implemented. It
is relevant as following sections tend to explain implementation details.

Typed Lua is a transpiler, it checks \code{.tl} files and compiles them to \code{.lua} files. From
the user perspective, compilation only removes type annotations and interface definitions. However the
resulting \code{.lua} files are entirely regenerated from the AST parsed from the \code{.tl} files. One
important thing to take into account is that Typed Lua is itself written in Lua, this allows Lua runtimes
to compile and run \code{.tl} files at runtime, which can be exciting at a first thought but
is quite silly if you consider that type checking hopefully appends ahead of time, it is otherwise
not very helpful. Having a compiler written in a script language is somewhat cumbersome.
As the motivation behind this project was to find solutions to write bigger software using Lua,
working into a huge Lua code-base \footnote{Several thousand lines per file. Files that, in addition, are poorly commented.} 
that process AST made the urge to improve Typed Lua even bigger.

To be able to give typed front end to already existing Lua libraries. Typed Lua also supports
\code{.tld} files. These are description files giving the type of the modules they describe.
They are also useful to hide untypable details into Lua files and only write the front end. 

Here is a list of Typed Lua compiler modules :

\begin{verbatim}
-loader.lua
-tlast.lua
-tlchecker.lua
-tlcode.lua
-tldparser.lua
-tlfilter.lua
-tllexer.lua
-tlparser.lua
-tlst.lua
-tltype.lua
-tlvisitor.lua
\end{verbatim}

The interesting files for this project are described hereafter.

\paragraph{\code{tlparser}} handles the parsing of the \code{.tl} files. The parsing uses the
\code{LPEG} (Lua Parsing Expression Grammar) library. Which makes the modification and upgrade of the
language grammar convenient and concise.


\paragraph{\code{tldparser}} does the parsing of the \code{.tld} (Typed Lua Description) files.
\paragraph{\code{tltype}} defines the types data structures and utilities to manipulate them.
Sub-typing relations and many crucial routines are defined there.
\paragraph{\code{tlast}} defines the abstract syntax tree structure along methods to traverse it and
match nodes. The abstract syntax tree design comes from the \emph{Meta-lua} project and reflects perfectly
the Lua syntax.
\paragraph{\code{tlst}} defines and manipulates the environment data structure. This defines scopes
management, local variable types, available symbols and help to check masking, unused variable and
type projection for the flow typing.